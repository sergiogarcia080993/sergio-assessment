package com.weather.controller;

import com.weather.model.Weather;
import com.weather.model.WeatherResponse;
import com.weather.service.api.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${application.core.api.base}")
public class WeatherController {

    private static final Logger logger = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    private WeatherService weatherService;

    @GetMapping(value = "/weather")
    public ResponseEntity<List<WeatherResponse>> getWeather(@RequestParam Float Latitude, @RequestParam Float Longitude){
        logger.info(".: We gonna start to get weather!");
        if(Latitude ==null || Longitude == null){
            return new ResponseEntity("Error", HttpStatus.BAD_REQUEST);
        }
        List<WeatherResponse> weatherResponseList = new ArrayList<>();
        WeatherResponse weatherCurrent = weatherService.getCurrentWeahter(Latitude,Longitude);
        weatherResponseList.add(weatherCurrent);
        //WeatherResponse weatherLastYear = weatherService.getLastYearWeather(Latitude,Longitude,System.currentTimeMillis());//Convert to epochdate
        //weatherResponseList.add(weatherLastYear);
        logger.info(".: END!");
        return new ResponseEntity<>(weatherResponseList, HttpStatus.CREATED);
    }
}
