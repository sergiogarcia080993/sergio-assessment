package com.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
    private float latitude;
    private float longitude;
    private String timezone;
    Currently currently;
    Daily daily;
    private float offset;

    // Getter Methods

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public Currently getCurrently() {
        return currently;
    }


    public Daily getDaily() {
        return daily;
    }


    public float getOffset() {
        return offset;
    }

    // Setter Methods

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setCurrently(Currently currentlyObject) {
        this.currently = currentlyObject;
    }



    public void setDaily(Daily dailyObject) {
        this.daily = dailyObject;
    }


    public void setOffset(float offset) {
        this.offset = offset;
    }
}
