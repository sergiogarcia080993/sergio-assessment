package com.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Minutely {
    private String summary;
    private String icon;
    ArrayList<Object> data = new ArrayList<Object>();


    // Getter Methods

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    // Setter Methods

    public void setSummary( String summary ) {
        this.summary = summary;
    }

    public void setIcon( String icon ) {
        this.icon = icon;
    }
}
