package com.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Daily {
    private String summary;
    private String icon;
    ArrayList<Data> data = new ArrayList<Data>();

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }
// Getter Methods

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    // Setter Methods

    public void setSummary( String summary ) {
        this.summary = summary;
    }

    public void setIcon( String icon ) {
        this.icon = icon;
    }
}