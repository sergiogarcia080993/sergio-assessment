package com.weather.model;

import java.sql.Date;

public class WeatherResponse {
    private Date date;
    private float time;
    private Float Ttemperature;
    private float sunriseTime;
    private float sunsetTime;
    private float temperatureHigh;
    private float temperatureHighTime;
    private float temperatureLow;
    private float temperatureLowTime;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public Float getTtemperature() {
        return Ttemperature;
    }

    public void setTtemperature(Float ttemperature) {
        Ttemperature = ttemperature;
    }

    public float getSunriseTime() {
        return sunriseTime;
    }

    public void setSunriseTime(float sunriseTime) {
        this.sunriseTime = sunriseTime;
    }

    public float getSunsetTime() {
        return sunsetTime;
    }

    public void setSunsetTime(float sunsetTime) {
        this.sunsetTime = sunsetTime;
    }

    public float getTemperatureHigh() {
        return temperatureHigh;
    }

    public void setTemperatureHigh(float temperatureHigh) {
        this.temperatureHigh = temperatureHigh;
    }

    public float getTemperatureHighTime() {
        return temperatureHighTime;
    }

    public void setTemperatureHighTime(float temperatureHighTime) {
        this.temperatureHighTime = temperatureHighTime;
    }

    public float getTemperatureLow() {
        return temperatureLow;
    }

    public void setTemperatureLow(float temperatureLow) {
        this.temperatureLow = temperatureLow;
    }

    public float getTemperatureLowTime() {
        return temperatureLowTime;
    }

    public void setTemperatureLowTime(float temperatureLowTime) {
        this.temperatureLowTime = temperatureLowTime;
    }
}
