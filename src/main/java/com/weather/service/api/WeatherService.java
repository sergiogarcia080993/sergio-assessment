package com.weather.service.api;

import com.weather.model.Weather;
import com.weather.model.WeatherResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

public interface WeatherService {

    public WeatherResponse getCurrentWeahter(float latitude, float longitude);

    public WeatherResponse getLastYearWeather(float latitude, float longitude, long epochtime);
}
