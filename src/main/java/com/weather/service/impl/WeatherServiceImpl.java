package com.weather.service.impl;

import com.weather.model.Weather;
import com.weather.model.WeatherResponse;
import com.weather.service.api.WeatherService;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.sql.Date;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Value("${darksky.url}")
    private String url;

    @Value("${darksky.secretkey}")
    private String secretKey;

    @Override
    public WeatherResponse getCurrentWeahter(float latitude, float longitude) {
        Client client = ClientBuilder.newClient().register(new JacksonFeature());
        Weather weather = client.target(url + secretKey + "/" + latitude + "," + longitude).request(MediaType.APPLICATION_JSON).get(Weather.class);
        return retrieveData(weather);
    }

    @Override
    public WeatherResponse getLastYearWeather(float latitude, float longitude, long epochtime) {
        Client client = ClientBuilder.newClient().register(new JacksonFeature());
        Weather weather = client.target(url + secretKey + "/" + latitude + "," +
                longitude + "," + epochtime).request(MediaType.APPLICATION_JSON).get(Weather.class);
        return retrieveData(weather);
    }

    public WeatherResponse retrieveData(Weather weather) {
        WeatherResponse response = new WeatherResponse();
        response.setDate(new Date((long) weather.getCurrently().getTime()));
        response.setTime(weather.getCurrently().getTime());
        response.setSunriseTime(weather.getDaily().getData().get(0).getSunriseTime());
        response.setSunsetTime(weather.getDaily().getData().get(0).getSunsetTime());
        response.setTemperatureHigh(weather.getDaily().getData().get(0).getTemperatureHigh());
        response.setTtemperature(weather.getCurrently().getTemperature());
        response.setTemperatureHighTime(weather.getDaily().getData().get(0).getTemperatureHighTime());
        response.setTemperatureLow(weather.getDaily().getData().get(0).getTemperatureLow());
        response.setTemperatureLowTime(weather.getDaily().getData().get(0).getApparentTemperatureLowTime());
        return response;
    }


}
