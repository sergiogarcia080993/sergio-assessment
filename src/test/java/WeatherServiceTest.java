import com.weather.model.WeatherResponse;
import com.weather.service.api.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class WeatherServiceTest {

    @Mock
    private WeatherService weatherService;

    @Test
    public void currentWeatherTest_OK() {
        WeatherResponse response = new WeatherResponse();
        response.setTemperatureLow((float) 5.3);
        response.setTemperatureLow(22);
        response.setTemperatureLowTime(12344);
        response.setSunsetTime(123466);
        when(weatherService.getCurrentWeahter(1L, 1L)).thenReturn(response);

    }

    @Test(expected = NullPointerException.class)
    public void currentWeatherTest_BadRequest() {
        WeatherResponse response = null;
        NullPointerException badRequestException = new NullPointerException("HTTP 400 Bad Request");
        Float latitude = null;
        when(weatherService.getCurrentWeahter(latitude, 1L)).thenThrow(badRequestException);

    }
}
